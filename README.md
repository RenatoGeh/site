# FLUSP's website

### How to run locally

First, install the dependencies:

```
$ bundle install
```

Then:

```
$ bundle exec jekyll serve
```

### How to contribute

Contributions should be sent via merge request.

New posts should be sent as files on the \_posts dir, with the following file
name rule: `YYYY-MM-DD-project-subproject-camel_cased_post_title.markdown`. And
each post file should contain the following header:

```
---
layout: post
title:  "project: subproject: Your Title Here"
date:   YYYY-MM-DD HH:MM:SS timezone (ex: -0300)
categories: comma,separated,categories
---
```

Note that the `project` and `subproject` prefixes must only me present when the
post refer to a specific project and/or subproject. A possible title for
a kernel iio post, e.g., could be: `kernel: iio: Playing with the iio-dummy`.
And a possible title for a general kernel post, e.g., could be:
`kernel: How to get maintainers for your patch`.

Also, feel free to contribute in other parts of the site and the overall
structure.

### Based on

- [Contrast](https://github.com/niklasbuschmann/contrast)
- [Jekyll](https://jekyllrb.com/)
- [Source Sans Pro](https://fonts.google.com/specimen/Source+Sans+Pro)
- [Font Awesome](http://fontawesome.io/)
- [Pygments](https://github.com/richleland/pygments-css)
- [Pixyll](https://github.com/johnotander/pixyll)

## Add new projects

If you want to add (or update) a new project, you have to add a new file at
``_pages/projects/`` directory and following the pattern below:

```
touch _pages/projects/<ID>-<PROJECT_NAME>.md
```

Just take a look at the other project files in order to have some examples.
