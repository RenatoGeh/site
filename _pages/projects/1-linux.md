---
title:  "Linux Kernel"
image:  "tux"
ref: "kernel"
categories: "projects.md"
id: 1
---

# Description

The Linux Kernel

# Recommendations

* Comfortable with C and pointers
* Bitwise operations
* Big Endian vs Little Endian
* Makefile (basic knowledge of)
* Git
* Comfortable with terminal and command line interface
* Desire to read/write low level code (e.g. reading datasheets is a common activity)"

# URLs

* [Source](https://git.kernel.org/pub/scm/linux/kernel/git)
* [IIO](https://git.kernel.org/pub/scm/linux/kernel/git/jic23/iio.git/)
